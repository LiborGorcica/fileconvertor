# FileConvertor
FileConvertor is a dotnet core console application that allows the user to convert files from one format to another. The application supports various file storages and formats.

# Installation
  * Build the application
  * Run the console application

# Example usages
## Convert on Local Storage
```
.\FileConvertor.exe --source LocalStorage --sourceUri C:\pathToFileConvertorApp\fileconvertor\FileConvertor.Tests\TestingFiles\test.yaml --destination LocalStorage --destinationUri C:\temp\testing\t.json
```

## Convert on Cloud Mock
 ```
 .\FileConvertor.exe --source Cloud --sourceUri https://mockstorage/input.json --destination Cloud --destinationUri https://mockStorage/out.xml --apiKey "secretApiKey"
 ```