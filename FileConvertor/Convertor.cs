﻿using FileConvertor.Deserializers;
using FileConvertor.FileReaders;
using System;
using System.Collections.Generic;
using System.Text;
using FileConvertor.FileWriters;
using FileConvertor.Model;
using FileConvertor.Serializers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FileConvertor
{
    public class Convertor<T>
    {
        private readonly IFileReader fileReader;
        private readonly IDeserializer<T> deserializer;
        private readonly ISerializer<T> serializer;
        private readonly IFileWriter fileWriter;
        private readonly Options options;
        private readonly ILogger<Convertor<T>> logger;

        public Convertor(IFileReader fileReader, IDeserializer<T> deserializer, ISerializer<T> serializer, IFileWriter fileWriter, Options options, ILogger<Convertor<T>> logger)
        {
            this.fileReader = fileReader;
            this.deserializer = deserializer;
            this.serializer = serializer;
            this.fileWriter = fileWriter;
            this.options = options;
            this.logger = logger;
        }

        public void Convert()
        {
            var content = ReadFile(options.SourceUri);
            var deserializedObject = DeserializeObject(content);
            var serialized = SerializeObject(deserializedObject);
            WriteFile(serialized, options.DestinationUri);
        }

        private string ReadFile(Uri sourceUri)
        {
            try
            {
                logger.LogInformation($"Reading file from {sourceUri} using {fileReader.GetType()}");
                return fileReader.Read(options.SourceUri);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"An exception has been throw while reading the file from {sourceUri} using {fileReader.GetType()}");
                throw;
            }
        }

        private T DeserializeObject(string content)
        {
            try
            {
                logger.LogInformation($"Deserializing the content using {deserializer.GetType()}");
                return deserializer.Deserialize(content);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"An exception has been throw while deserializing the content using {deserializer.GetType()}");
                throw;
            }
        }

        private string SerializeObject(T document)
        {
            try
            {
                logger.LogInformation($"Serializing the object using {serializer.GetType()}");
                return serializer.Serialize(document);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"An exception has been throw while serializing the object using {serializer.GetType()}");
                throw;
            }
        }

        private void WriteFile(string serialized, Uri destinationUri)
        {
            try
            {
                logger.LogInformation($"Writing file to {destinationUri} using {fileWriter.GetType()}");
                fileWriter.Write(options.DestinationUri, serialized);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"An exception has been throw while writing the file to {destinationUri} using {fileWriter.GetType()}");
                throw;
            }
        }
    }
}
