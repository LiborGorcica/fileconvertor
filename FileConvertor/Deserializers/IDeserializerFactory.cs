﻿namespace FileConvertor.Deserializers
{
    public interface IDeserializerFactory<T>
    {
        public IDeserializer<T> Get();
    }
}