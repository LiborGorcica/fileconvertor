﻿using System;
using System.Collections.Generic;
namespace FileConvertor.Deserializers
{
    public interface IDeserializer<T>
    {
        T Deserialize(string input);
    }
}
