﻿using FileConvertor.Model;
using System.Xml.Linq;

namespace FileConvertor.Deserializers
{
    public class XmlDeserializer : IDeserializer<Document>
    {
        public Document Deserialize(string input)
        {
            var xdoc = XDocument.Parse(input);

            return new Document
            {
                Title = xdoc.Root?.Element("Title")?.Value ?? string.Empty,
                Text = xdoc.Root?.Element("Text")?.Value ?? string.Empty
            };
        }
    }
}
