﻿using FileConvertor.Model;
using Newtonsoft.Json;

namespace FileConvertor.Deserializers
{
    public class JsonDeserializer : IDeserializer<Document>
    {
        public Document Deserialize(string input)
        {
            return JsonConvert.DeserializeObject<Document>(input);
        }
    }
}
