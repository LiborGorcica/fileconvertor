﻿using FileConvertor.Model;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace FileConvertor.Deserializers
{
    public class DeserializerFactory : IDeserializerFactory<Document>
    {
        private IServiceProvider serviceProvider;
        private Options options;

        public DeserializerFactory(IServiceProvider serviceProvider, Options options)
        {
            this.serviceProvider = serviceProvider;
            this.options = options;
        }

        public IDeserializer<Document> Get()
        {
            var extension = new FileInfo(options.SourceUri.AbsolutePath).Extension;

            switch (extension.ToLower())
            {
                case ".json": return serviceProvider.GetService<JsonDeserializer>();
                case ".xml": return serviceProvider.GetService<XmlDeserializer>();
                case ".yaml": return serviceProvider.GetService<YamlDeserializer>();
                default: throw new InvalidOperationException("The specified source uri file type is not supported.");
            }
        }
    }
}