﻿using FileConvertor.Model;
using YamlDotNet.Serialization;

namespace FileConvertor.Deserializers
{
    public class YamlDeserializer : IDeserializer<Document>
    {
        public Document Deserialize(string input)
        {
            var deserializer = new DeserializerBuilder()
                .Build();

            return deserializer.Deserialize<Document>(input);
        }
    }
}
