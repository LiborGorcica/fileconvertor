﻿namespace FileConvertor.CloudServiceMock
{
    public interface ICloudService
    {
        string GetContent(string apiKey, string fileName);
        void WriteContent(string apiKey, string content, string fileName);
    }
}