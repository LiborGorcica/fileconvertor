﻿using System;

namespace FileConvertor.CloudServiceMock
{
    public class AuthenticationException : Exception
    {
        public AuthenticationException(string message) : base(message)
        {
            
        }
    }
}