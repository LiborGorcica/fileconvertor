﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace FileConvertor.CloudServiceMock
{
    public class CloudServiceMock : ICloudService
    {
        private const string ApiKey = "secretApiKey";
        private readonly ILogger<CloudServiceMock> logger;

        public CloudServiceMock(ILogger<CloudServiceMock> logger)
        {
            this.logger = logger;
        }

        public string GetContent(string apiKey, string fileName)
        {
            if(ApiKey != apiKey)
                throw new AuthenticationException("Authentication failed");

            return @"
{
    ""Title"": ""Cloun test title"",
    ""Text"": ""Test Text""
}";
        }

        public void WriteContent(string apiKey, string content, string fileName)
        {
            if (ApiKey != apiKey)
                throw new AuthenticationException("Authentication failed");

            logger.LogInformation($"Writing content {content}");
        }
    }
}