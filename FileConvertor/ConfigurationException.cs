﻿using System;

namespace FileConvertor
{
    public class ConfigurationException : Exception
    {
        public ConfigurationException(string message) : base(message) 
        {
                
        }
    }
}