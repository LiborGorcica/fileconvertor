﻿using System;
using System.IO.Abstractions;
using FileConvertor.CloudServiceMock;
using FileConvertor.Deserializers;
using FileConvertor.FileReaders;
using FileConvertor.FileWriters;
using FileConvertor.Model;
using FileConvertor.Serializers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FileConvertor
{
    public class IocContainer
    {
        public IServiceCollection Install(Options options)
        {
            return new ServiceCollection()
                .AddLogging(c => c.AddConsole())
                .AddSingleton<Options>(options)
                .AddTransient<FileSystemReader>()
                .AddTransient<CloudFileReader>()
                .AddTransient<CloudFileWriter>()
                .AddTransient<JsonDeserializer>()
                .AddTransient<JsonSerializer>()
                .AddTransient<YamlSerializer>()
                .AddTransient<XmlSerializer>()
                .AddTransient<XmlDeserializer>()
                .AddTransient<YamlDeserializer>()
                .AddTransient<FileSystemWriter>()
                .AddTransient<WebFileReader>()
                .AddTransient<IFileSystem, FileSystem>()
                .AddTransient<IFileReaderFactory, FileReaderFactory>()
                .AddTransient<IDeserializerFactory<Document>, DeserializerFactory>()
                .AddTransient<ISerializerFactory<Document>, SerializerFactory>()
                .AddTransient<IFileWriterFactory, FileWriterFactory>()
                .AddTransient<ICloudService, CloudServiceMock.CloudServiceMock>()
                .AddTransient<IFileReader>((sp) => sp.GetService<IFileReaderFactory>().Get())
                .AddTransient<IDeserializer<Document>>((sp) => sp.GetService<IDeserializerFactory<Document>>().Get())
                .AddTransient<ISerializer<Document>>((sp) => sp.GetService<ISerializerFactory<Document>>().Get())
                .AddTransient<IFileWriter>((sp) => sp.GetService<IFileWriterFactory>().Get())
                .AddTransient<Convertor<Document>>();
        }
    }
}