﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace FileConvertor.FileReaders
{
    public class WebFileReader : IFileReader
    {
        public string Read(Uri uri)
        {
            using (var webClient = new WebClient())
            {
                return webClient.DownloadString(uri);
            }
        }
    }
}
