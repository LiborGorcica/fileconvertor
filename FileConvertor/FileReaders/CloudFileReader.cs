﻿using System;
using System.IO;
using FileConvertor.CloudServiceMock;

namespace FileConvertor.FileReaders
{
    public class CloudFileReader : IFileReader
    {
        private readonly ICloudService cloudService;
        private readonly Options options;

        public CloudFileReader(ICloudService cloudService, Options options)
        {
            this.cloudService = cloudService;
            this.options = options;
        }

        public string Read(Uri uri)
        {
            if (string.IsNullOrEmpty(options.ApiKey))
            {
                throw new ConfigurationException("ApiKey parameter must be specified");
            }

            var fileName = new FileInfo(uri.AbsolutePath).Name;
            return cloudService.GetContent(options.ApiKey, fileName);
        }
    }
}