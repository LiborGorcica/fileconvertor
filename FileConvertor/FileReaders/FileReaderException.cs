﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileConvertor.FileReaders
{
    public class FileReaderException : Exception
    {
        public FileReaderException(string message) : base(message)
        {

        }
    }
}
