﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace FileConvertor.FileReaders
{
    public class FileReaderFactory : IFileReaderFactory
    {
        private IServiceProvider serviceProvider;
        private Options options;

        public FileReaderFactory(IServiceProvider serviceProvider, Options options)
        {
            this.serviceProvider = serviceProvider;
            this.options = options;
        }

        public IFileReader Get()
        {
            switch (options.Source)
            {
                case Source.LocalStorage:
                    return serviceProvider.GetService<FileSystemReader>();
                case Source.Cloud:
                    return serviceProvider.GetService<CloudFileReader>();
                case Source.Web:
                    return serviceProvider.GetService<WebFileReader>();
                default:
                    throw new ArgumentOutOfRangeException($"The specified source {options.Source} is not supported.");
            }
        }
    }
}