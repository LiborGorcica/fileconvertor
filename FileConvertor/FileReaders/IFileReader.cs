﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileConvertor.FileReaders
{
    public interface IFileReader
    {
        string Read(Uri uri);
    }
}
