﻿using System;
using System.IO;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Text;

namespace FileConvertor.FileReaders
{
    public class FileSystemReader : IFileReader
    {
        private IFileSystem fileSystem;

        public FileSystemReader(IFileSystem fileSystem)
        {
            this.fileSystem = fileSystem;
        }

        public string Read(Uri uri)
        {
            if(!uri.IsFile)
            {
                throw new FileReaderException("The specified uri is not a file URI");
            }

            var sourceFile = fileSystem.FileInfo.FromFileName(uri.LocalPath);
            if(!sourceFile.Exists)
            {
                throw new FileReaderException($"The specified file {sourceFile.FullName} does not exists.");
            }

            return fileSystem.File.ReadAllText(sourceFile.FullName);
        }
    }
}
