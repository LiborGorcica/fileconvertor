﻿namespace FileConvertor.FileReaders
{
    public interface IFileReaderFactory
    {
        public IFileReader Get();
    }
}