﻿using System.IO;
using System.Text;
using System.Xml;
using FileConvertor.Model;
using YamlDotNet.Serialization;

namespace FileConvertor.Serializers
{
    public class YamlSerializer : ISerializer<Document>
    {
        public string Serialize(Document input)
        {
            var serializer = new SerializerBuilder().Build();
            return serializer.Serialize(input);
        }
    }
}