﻿namespace FileConvertor.Serializers
{
    public interface ISerializerFactory<T>
    {
        ISerializer<T> Get();
    }
}