﻿using FileConvertor.Model;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace FileConvertor.Serializers
{
    public class SerializerFactory : ISerializerFactory<Document>
    {
        private IServiceProvider serviceProvider;
        private Options options;

        public SerializerFactory(IServiceProvider serviceProvider, Options options)
        {
            this.serviceProvider = serviceProvider;
            this.options = options;
        }

        public ISerializer<Document> Get()
        {
            var extension = new FileInfo(options.DestinationUri.AbsolutePath).Extension;

            switch (extension.ToLower())
            {
                case ".json": return serviceProvider.GetService<JsonSerializer>();
                case ".xml": return serviceProvider.GetService<XmlSerializer>();
                case ".yaml": return serviceProvider.GetService<YamlSerializer>();
                default: throw new InvalidOperationException("The specified destination uri file type is not supported.");
            }
        }
    }
}