﻿using System.IO;
using System.Text;
using System.Xml;
using FileConvertor.Model;

namespace FileConvertor.Serializers
{
    public class XmlSerializer : ISerializer<Document>
    {
        public string Serialize(Document input)
        {
            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Document));
            
            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, input);
                return textWriter.ToString();
            }
        }
    }
}