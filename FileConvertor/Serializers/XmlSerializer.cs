﻿using System.IO;
using System.Text;
using System.Xml;
using FileConvertor.Model;
using Newtonsoft.Json;

namespace FileConvertor.Serializers
{
    public class JsonSerializer : ISerializer<Document>
    {
        public string Serialize(Document input)
        {
            return JsonConvert.SerializeObject(input);
        }
    }
}