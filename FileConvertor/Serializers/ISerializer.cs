﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FileConvertor.Model;

namespace FileConvertor.Serializers
{
    public interface ISerializer<T>
    {
        string Serialize(T input);
    }
}
