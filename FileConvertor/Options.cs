﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileConvertor
{
    public enum Source
    {
        LocalStorage,
        Cloud,
        Web
    };

    public enum Destination
    {
        LocalStorage,
        Cloud
    };


    //https://github.com/commandlineparser/commandline
    public class Options
    {
        [Option('s', "source", Required = true, HelpText = "Source storage of the file to be converted. Possible values: LocalStorage | Cloud | Web")]
        public Source Source { get; set; }
        [Option('o', "sourceUri", Required = true, HelpText = "Uri of the source file.")]
        public Uri SourceUri { get; set; }
        [Option('d', "destination", Required = true, HelpText = "Destination storage for the converted file.")]
        public Destination Destination { get; set; }
        [Option('e', "destinationUri", Required = true, HelpText = "Uri of the destination file. Possible values: LocalStorage | Cloud")]
        public Uri DestinationUri { get; set; }
        [Option('a', "apiKey", HelpText = "Api key for the cloud storage. Use 'secretApiKey' value for testing purposes.")]
        public string ApiKey { get; set; }
    }
}
