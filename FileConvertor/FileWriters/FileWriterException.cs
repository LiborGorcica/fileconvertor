﻿using System;

namespace FileConvertor.FileWriters
{
    public class FileWriterException : Exception
    {
        public FileWriterException(string message) : base(message)
        {
                
        }
    }
}