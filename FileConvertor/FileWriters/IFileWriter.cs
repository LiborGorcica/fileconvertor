﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileConvertor.FileWriters
{
    public interface IFileWriter
    {
        void Write(Uri destination, string content);
    }
}
