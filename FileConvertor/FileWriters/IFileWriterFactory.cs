﻿namespace FileConvertor.FileWriters
{
    public interface IFileWriterFactory
    {
        IFileWriter Get();
    }
}