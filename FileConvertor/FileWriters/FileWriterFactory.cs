﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace FileConvertor.FileWriters
{
    public class FileWriterFactory : IFileWriterFactory
    {
        private IServiceProvider serviceProvider;
        private Options options;

        public FileWriterFactory(IServiceProvider serviceProvider, Options options)
        {
            this.serviceProvider = serviceProvider;
            this.options = options;
        }

        public IFileWriter Get()
        {
            switch (options.Destination)
            {
                case Destination.LocalStorage:
                    return serviceProvider.GetService<FileSystemWriter>();
                case Destination.Cloud:
                    return serviceProvider.GetService<CloudFileWriter>();
                default:
                    throw new ArgumentOutOfRangeException($"The specified destination {options.Destination} is not supported.");
            }
        }
    }
}