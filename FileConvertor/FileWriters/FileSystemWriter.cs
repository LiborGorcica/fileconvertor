﻿using System;
using System.IO;
using System.IO.Abstractions;

namespace FileConvertor.FileWriters
{
    public class FileSystemWriter : IFileWriter
    {
        private readonly IFileSystem fileSystem;

        public FileSystemWriter(IFileSystem fileSystem)
        {
            this.fileSystem = fileSystem;
        }

        public void Write(Uri destination, string content)
        {
            if (!destination.IsFile)
            {
                throw new FileWriterException("The specified uri is not a file URI");
            }

            fileSystem.File.WriteAllText(destination.LocalPath, content);
        }
    }
}