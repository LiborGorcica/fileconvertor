﻿using System;
using System.IO;
using FileConvertor.CloudServiceMock;
using FileConvertor.FileWriters;

namespace FileConvertor.FileWriters
{
    public class CloudFileWriter : IFileWriter
    {
        private readonly ICloudService cloudService;
        private readonly Options options;

        public CloudFileWriter(ICloudService cloudService, Options options)
        {
            this.cloudService = cloudService;
            this.options = options;
        }

        public void Write(Uri uri, string content)
        {
            if (string.IsNullOrEmpty(options.ApiKey))
            {
                throw new ConfigurationException("ApiKey parameter must be specified");
            }

            var fileName = new FileInfo(uri.AbsolutePath).Name;
            cloudService.WriteContent(options.ApiKey, content, fileName);
        }
    }
}