﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileConvertor.Model
{
    public class Document
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
