﻿using CommandLine;
using FileConvertor.Deserializers;
using FileConvertor.FileReaders;
using FileConvertor.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.IO.Abstractions;
using System.Net;
using FileConvertor.CloudServiceMock;
using FileConvertor.FileWriters;
using FileConvertor.Serializers;

namespace FileConvertor
{
    public class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                   .WithParsed<Options>(o =>
                   {
                       ServiceProvider serviceProvider = null;
                       try
                       {
                           var iocContainer = new IocContainer();

                           serviceProvider = iocContainer.Install(o).BuildServiceProvider();

                           var processFile = serviceProvider.GetService<Convertor<Document>>();
                           processFile.Convert();
                       }
                       catch (Exception e)
                       {
                           Console.WriteLine(
                               "Unfortunately there was an exception while converting the file. See the logs for details");

                           if (serviceProvider == null)
                           {
                               Console.WriteLine($"Message: {e.Message}, Stack: {e.StackTrace}");
                               return;
                           }

                           var logger = serviceProvider.GetService<ILogger<Program>>();

                           logger.LogError(e, "Unexpected exception.");
                           return;
                       }
                       finally
                       {
                           serviceProvider?.Dispose();
                       }
                   });
        }
    }
}
