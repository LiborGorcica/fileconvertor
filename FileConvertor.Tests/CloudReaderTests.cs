﻿using System;
using FileConvertor.CloudServiceMock;
using FileConvertor.FileReaders;
using Moq;
using NUnit.Framework;

namespace FileConvertor.Tests
{
    public class CloudReaderTests
    {
        private Mock<ICloudService> cloudServiceMock;
        private Options testingOptions;

        [SetUp]
        public void Setup()
        {
            cloudServiceMock = new Mock<ICloudService>();
            testingOptions = GetTestingOptions();
        }

        [Test]
        public void TestRead()
        {
            var cloudReader = new CloudFileReader(cloudServiceMock.Object, testingOptions);

            cloudServiceMock.Setup(c => c.GetContent(It.IsAny<string>(), It.IsAny<string>())).Returns("content");

            cloudReader.Read(new Uri("https://testcloud/file.json"));

            cloudServiceMock.Verify(c => c.GetContent(testingOptions.ApiKey, "file.json"));
        }

        [Test]
        public void ShouldThrowIfApiKeyIsNotSpecified()
        {

            var cloudReader = new CloudFileReader(cloudServiceMock.Object, new Options());

            cloudServiceMock.Setup(c => c.GetContent(It.IsAny<string>(), It.IsAny<string>())).Returns("content");

            var exception = Assert.Throws<ConfigurationException>(() => cloudReader.Read(new Uri("https://testcloud/file.json")));
            Assert.That(exception.Message, Is.EqualTo("ApiKey parameter must be specified"));
        }

        private Options GetTestingOptions()
        {
            return new Options()
            {
                ApiKey = "testKey"
            };
        }
    }
}