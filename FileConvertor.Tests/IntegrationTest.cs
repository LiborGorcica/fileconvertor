﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using FileConvertor.Deserializers;
using FileConvertor.FileReaders;
using FileConvertor.Model;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace FileConvertor.Tests
{
    public class IntegrationTest
    {
        private ServiceProvider serviceProvider;
        private Options options;

        [SetUp]
        public void Setup()
        {
            var iocContainer = new IocContainer();
            options = GetTestingOptions();
            var serviceCollection = iocContainer.Install(options);
            var descriptor =
                new ServiceDescriptor(
                    typeof(IFileSystem),
                    typeof(FileSystem),
                    ServiceLifetime.Transient);
            serviceCollection.Remove(descriptor);

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { options.SourceUri.LocalPath, new MockFileData(GetTestFileContent()) },
            });

            serviceCollection.AddTransient<IFileSystem, MockFileSystem>((s) => fileSystem);
            serviceProvider = serviceCollection.BuildServiceProvider();
        }

        [Test]
        public void Convert_FileReaderYaml()
        {
            var convertor = serviceProvider.GetService<Convertor<Document>>();
            var fileSystem = serviceProvider.GetService<IFileSystem>();
            var fileReader = serviceProvider.GetService<IFileReader>();
            var xmlDeserializer = serviceProvider.GetService<XmlDeserializer>();

            convertor.Convert();
            Assert.That(() => fileSystem.File.Exists(options.DestinationUri.LocalPath));
            var content = fileReader.Read(options.DestinationUri);
            var document = xmlDeserializer.Deserialize(content);

            Assert.That(() => document.Title, Is.EqualTo("Test title"));
            Assert.That(() => document.Text, Is.EqualTo("Test text"));
        }

        private Options GetTestingOptions()
        {
            return new Options()
            {
                DestinationUri = new Uri("T:/destinationTest.xml"),
                SourceUri = new Uri("T:/sourceTest.json"),
                Source = Source.LocalStorage,
                Destination = Destination.LocalStorage
            };
        }

        private string GetTestFileContent()
        {
            return @"
{
    ""Title"": ""Test title"",
    ""Text"": ""Test text""
}";
        }
    }
}