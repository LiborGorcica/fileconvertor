using System;
using FileConvertor.Deserializers;
using FileConvertor.FileReaders;
using FileConvertor.FileWriters;
using FileConvertor.Serializers;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace FileConvertor.Tests
{
    public class Tests
    {
        private Mock<IFileReader> fileReaderMock;
        private Mock<IDeserializer<TestDocument>> deserializerMock;
        private Mock<ISerializer<TestDocument>> serializerMock;
        private Mock<IFileWriter> fileWriterMock;
        private Mock<ILogger<Convertor<TestDocument>>> loggerMock;

        [SetUp]
        public void Setup()
        {
            fileReaderMock = new Mock<IFileReader>();
            deserializerMock = new Mock<IDeserializer<TestDocument>>();
            serializerMock = new Mock<ISerializer<TestDocument>>();
            fileWriterMock = new Mock<IFileWriter>();
            loggerMock = new Mock<ILogger<Convertor<TestDocument>>>();
        }

        [Test]
        public void TestConvertingWorkflow()
        {
            var testingOptions = GetTestingOptions();
            var testDocument = new TestDocument() {Name = "TestContent"};
            fileReaderMock.Setup(a => a.Read(It.IsAny<Uri>())).Returns("TestContent");
            deserializerMock.Setup(a => a.Deserialize(It.IsAny<string>())).Returns(testDocument);
            serializerMock.Setup(a => a.Serialize(It.IsAny<TestDocument>())).Returns("serialized");
            fileWriterMock.Setup(a => a.Write(It.IsAny<Uri>(), "serialized"));

            var convertor = new Convertor<TestDocument>(fileReaderMock.Object, deserializerMock.Object, serializerMock.Object, fileWriterMock.Object, testingOptions, loggerMock.Object);

            convertor.Convert();

            fileReaderMock.Verify(f => f.Read(testingOptions.SourceUri), Times.Once);
            deserializerMock.Verify(d => d.Deserialize("TestContent"), Times.Once);
            serializerMock.Verify(s => s.Serialize(testDocument), Times.Once);
            fileWriterMock.Verify(v => v.Write(testingOptions.DestinationUri, "serialized"), Times.Once);
        }

        private Options GetTestingOptions()
        {
            return new Options()
            {
                DestinationUri = new Uri("Test:/destinationTest.xml"),
                SourceUri = new Uri("Test:/sourceTest.yaml")
            };
        }
    }
}